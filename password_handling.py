import hashlib
import os
#from symbol import stmt
import apsw
import sys
from apsw import Error


def pwHashing(input, salt):
    """
    It takes a input(string) and a salt, and returns a hash of the string using the salt
    
    :param input: The password to be hashed
    :param salt: A random string of characters that is used to make the hash more secure
    :return: The hash of the input string.
    """
    return hashlib.pbkdf2_hmac('sha256', input.encode('utf-8'), salt, 100000)

def getUserInfo(user_id):
    """
    It takes the user_id as an argument, and returns a list of tuples containing the username, password,
    and salt of the user with the given user_id.
    
    :param user_id: The username of the user
    :return: A list of tuples.
    """
    stmt = ('SELECT username, password, salt FROM users WHERE username GLOB ?')
    cn = conn.execute(stmt, [(user_id)])
    return cn.fetchall()    

#Setting up password for a user
def addingNewUser(user_id, password):
    """
    It takes a user_id and password, checks if the user_id is already in the database, and if not, adds
    it to the database.
    
    :param user_id: the username of the user
    :param password: the password the user entered
    """
    rows = getUserInfo(user_id)
    if len(rows) == 0:
        salt = os.urandom(32)
        key = pwHashing(password, salt)
        stmt = ('INSERT INTO users(username, password, salt) values (?,?,?)')
        try:
            conn.execute(stmt, [user_id, key, salt])
        except Error as e:
            print(e)
            conn.rollback()
            sys.exit(1)


#checks if input user matches name and pw in DB.
def check_pw(user_id, inputPass):
    """
    If the user_id exists in the database, and the password matches the hashed password in the database,
    return True. Otherwise, return False
    
    :param user_id: the user's id
    :param inputPass: the password the user entered
    :return: A boolean value.
    """
    userInfo = getUserInfo(user_id)
    if len(userInfo) == 0:
        return False
    if userInfo[0][0] == user_id and userInfo[0][1] == pwHashing(inputPass, userInfo[0][2]):
        return True
    return False



def getUserId(user_id):
    """
    It takes a username as an argument, and returns the user's id.
    
    :param user_id: The user's username
    :return: The user_id
    """
    stmt = ('SELECT id FROM users WHERE username = ?')
    c = conn.execute(stmt, user_id)
    c = c.fetchall()[0][0]
    return c


def createNewConvo(convoName, UsersList):
    """
    It takes a list of users and creates a new conversation with those users.
    
    :param convoName: The name of the conversation
    :param UsersList: ['user1', 'user2', 'user3']
    :return: The convoID of the newly created conversation.
    """
    try:
        stmt = ('INSERT INTO convo_id(name) values (?) RETURNING convo')
        convoID = conn.execute(stmt, [(convoName)]).fetchall()[0][0]
        stmt = (f'INSERT INTO usersOnline (convo, user) values ({convoID}, ?)')
        UserIDs = [[getUserId(x)] for x in UsersList]
        conn.executemany(stmt, (UserIDs))
        return convoID
    except Error as e:
        print(e)
        sys.exit(1)



 
def UsersInConvo(convoID):
    """
    It returns a list of all the users in a conversation.
    
    :param convoID: The ID of the conversation
    :return: A list of all the users in a conversation.
    """
    stmt = ('''SELECT users.username FROM users INNER JOIN usersOnline ON
    users.id = userinchats.user INNER JOIN convo_id ON convo_id.convo = usersOnline.convo
    WHERE convo_id.convo = ? ''')
    c = conn.execute(stmt, [convoID])
    list = [x[0] for x in c]
    return list

def getMsgFromConvo(convoID, user, searchword = '*'):
    if not user in UsersInConvo(convoID):
        return [f'User not in convo with this ID']
    if searchword !='*':
        searchword = '*' + searchword + '*'
    stmt = ('SELECT * FROM messages WHERE convoID GLOB ? AND message GLOB ? ')
    c = conn.execute(stmt, [convoID, searchword])
    c = c.fetchall()
    return c 

def getMyConvos(user_int_id):
    stmt = ('''SELECT convo_id.convo, convo_id.name FROM convo_ID INNER JOIN usersOnline
    ON usersOnline.convo = convo_id.convo WHERE usersOnline.user GLOB ? ''')
    c = conn.execute(stmt, [(user_int_id)])
    c = c.fetchall()
    return c

def searchMyConvos(user, searchword ='*'):
    if searchword != '*':
        searchword = '*' + searchword + '*'
    listOfConvos = getMyConvos(getUserId(user))
    toBeReturned = []
    for x in listOfConvos:
        convoID = x[0]
        next = getMsgFromConvo(convoID, user, searchword)
        if bool(next):
            toBeReturned.append(next)
    return toBeReturned

def sendMsg(sender, convoID, message):
    if not sender in UsersInConvo(convoID):
        return [f"user not in chat with corresponding ID"]
    stmt = ('INSERT INTO messages (chatID, sender, message) values (?,?,?)')
    conn.execute(stmt, [convoID, sender, message])
    return 'OK'
#Existing DB, replace with new ones.
try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY,
        convoID integer, 
        sender TEXT NOT NULL,
        receiver TEXT NOT NULL,
        message TEXT NOT NULL,
        timestamp DATETIME DEFAULT CURRENT_TIMESTAMP);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
    #new table, this can be done better, add some types to the different variables?
    c.execute('''CREATE TABLE IF NOT EXISTS users ( 
        id integer PRIMARY KEY,
        username TEXT NOT NULL,
        password BLOB,
        salt BLOB,
        token BLOB);''') #Remove token if necessary(Just added.)
    #New table for users  in conversation identification
    c.execute('''CREATE TABLE IF NOT EXISTS usersOnline (
        id integer PRIMARY KEY,
        convo integer,
        user integer);''')
    #New table for identifying specific conversations with chat-id
    c.execute('''CREATE TABLE IF NOT EXISTS convo_id(
        convo integer PRIMARY KEY,
        name TEXT NOT NULL);''')
    

except Error as e:
    print(e)
    sys.exit(1)


