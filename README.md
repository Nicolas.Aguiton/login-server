Mandatory Assignment 2

Make a note of what's wrong with the structure (and anything else you see that may be problematic) – could any of these things have a security impact?
Answer: Having all your code mixed into one file without any structure could in theory work, if we were talking about a small easy to read program, but in most cases, it is uncalled for. It is always better to have a well-structured code, meaning different files making it easier to navigate through it, this also helps if other people are to review or look at your code.


What could you have done if you had more time?
There were several things I really wanted to implement with the project, sadly it was quite time consuming considering also I had no experience with HTML/CSS/JS and just a slight understanding of flask from earlier. I believe I should have had correct/better fitting data types for each attribute, considering we are dealing with delicate data, having the correct data types is important for the security of our app/database. Having a password security check such as min. length, banning certain characters, and maybe requiring 1 capitalized letter and a symbol per password. Finish implementing the message app, so that everything is functional as desired. Better handling of cookies/tokens, making it possible to change users?
Refactor the code better and order how all the libraries/modules are imported since it is not pleasing to look at all now.

2B- Documentation
• a brief overview of your design considerations from Part A

The goal was improving the given login-server project which was somehow chaotic when it comes down to the code structure and it had quite the basic GUI, nothing fancy. I really tried to put as much time as I could to improve and implement as many things as I could, it was honestly such a fun project, kind of bitter I did not get to finish what I had in mind.
Cleaning or rather refactoring the code was quite the hassle, I tried doing it from the start, but it ended up backfiring and causing me quite the trouble to understand the given project, so I changed my mind and instead started by implementing and rather refactor in the way or at the end. I ended up following the quite standard flask project structure, meaning that I have a static folder with all the corresponding static files (This would refer to css files, icons and images, for example) every HTML file ended up being in the “template” folder as it is protocol.
	
I was quite stubborn when it came down to the fact that I thought it would only be logical to have a sign-up page in order to be able to log-in and use the messaging app, so that is what I started with (P.S I might have used just a tiny bit too much time on design, but hopefully it is appreciated.) Managed to make it so a user needs to fill out a password in order to register and tick the checkbox for the Terms and conditions, the username and password (secure Hash + salt) would then be stored in a database, redirecting the user into the login page where they would then put their newly made account into the test and use those credentials to log in. I at last started designing/implementing on the messaging app but had sadly rather little time, costing me an unfinished application.

• The features of your application

Description
I wanted to somehow have a similar application to Discord, or that was at least the inspiration. In the beginning the application was designed for every user to see all messages, I wanted to aggregate private messages as well, such that a user could decide from and “to” which user the message was going to be aimed at.

Signing up
This is the home page of the website/application where users would have to register in order to access the application. Both username and password are required fields, so the user would have to fill in a username and password in addition to agreeing to the ToS! If a user already has an account, it would be possible to just click on Sign in at the bottom of the page and be redirected to the login page. I wanted to implement some password quality or security measure’s check such as a minimum length to the password, certain symbols not being allowed, but did not have the time to do so.



Logging in
Improvements to the original login site (/login) made it so both fields are required to fill in order to log in and that log in is required in order to be redirected to the messaging app.



Message application
The message application is where we get redirected to right after logging in, the user can’t access it unless they are logged in and authenticated. In theory the user could search through their messages, start conversations, send public and private messages, sadly my message application is somewhat faulty, with some extra time this would have been no problem to implement.


Logging out
Designed a log out button in the messaging app, so the user might log out to restart the app, switch user or whatever the need might be. The user could either use the said log out button at t or access it through writing /logout on the url


• instructions on how to test/demo it
In order to test this development application/server the micro web framework “Flask” is required, we then run the either “flask run” or “python -m flask run” in the terminal to set up the connection to the server and the application, afterwards launch http://127.0.0.1:5000 or ‘localhost:5000’ to be redirected to our application.

After launching the application for the first time you will be redirected first of all to our “signup page” there will be no test users from before, so please create a test user and if you have already created an account just click on “Sign in” (Remember to agree to our terms of service!) please do have in mind that whichever username and password you give as input it will be saved in the corresponding table “users” in our database. The password will be hashed (with addition to having a salt) so no worries. Afterwards you will be redirected to our login page (Accessible with writing /login in the URL) where you will be able and required to login in order to finally get to our messaging app! Which will be accessible in theory by writing “/index” in the URL, only if you are already logged in though!



• technical details on the implementation
Most of the technology used in the project/application is used from the already known, login-server. Some extra libraries were required in order to hash the passwords, etc. 

• answers to the questions below
•  Threat model – who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?

The attacker who in like any case could be anyone could do a lot of harm to an unprepared and unfinished application, since the required and desired security practices are not applied, which could be translatable to the real world, small companies, start-ups, schools, etc. could have horribly made-up websites with very little security measures being taken in consideration, which is quite alarming
The attacker could look at some of these types of attacks:
Injection vulnerabilities, SQL injection, Command Injection Attacks, Cookies Stealing and manipulation and XSS for example.
Further explanation of why Cross-Site Scripting (XSS)is one of the most dangerous attack vectors we must worry about in this project. Shortly explained XSS is when an attacker is allowed to perform HTML injection, inserting their own HTML code into a web page, furthermore this could sound easy to avoid however an attacker could even embed their own HTML <SCRIPT></SCRIPT> containing malicious code into a link, for instance provide the victim with a link renamed to “Check your account at First Bank” which will redirect the victim to the authentic website with the proper URL address and toolbar and even a valid digital certificate, However, the website would then execute the malicious code(function) inside the SCRIPT included in the input by the attacker. 
INPUT VALIDATION should be one of the main concerns for developers to ensure code being secure, improper input handling can expose the application to injection attacks like the ones mentioned before.

SQL Injection could be the perfect example on what damage could be done in terms of the CIA triad, the attacker could breach the whole three by instance if the attacker gets their hands on sensitive information (as a user’s password from the DB) that would be a breach of Confidentiality, changing/manipulating data from the database would be a breach of Integrity and at last they could delete certain information/data causing a loss of availability. 


•  What are the main attack vectors for the application? 
The main attack vectors are most definitely the big possibility of SQL Injection (SQLI), Cross-Site Scripting(XSS), and Cross Site Request Forgery(CSFR), perhaps DDoS to the server as well?
•  What should we do (or what have you done) to protect against attacks? 
There are several things that could have been done and were done. Adding a sense of authentication making it so that users were forced to register and log in to use the app, removing the possibility of changing between users without any authentication, which could lead to phishing and even social engineering gimmicks. Hash + salting the passwords, and not being able to look at them since they are encrypted, prep statements as well all in order to prevent SQL-Injection. 

•  What is the access control model? 
The original application had very few limitations, any user could practically do whatever they wanted, by for instance being able to see all messages without any restrictions, the SQL queries behind every message request was visible to every user as well, this is obviously faulty or rather incomplete security measures. 
In my application I strived to remove anything that could be a liability away from the front-end of the app, meaning removing the SQL queries behind the application for instance. You are required to sign-up and login afterwards in order to enjoy the application. Adding private messages and those messages being exclusively available to the users they were intended for was a nice implementation as well. I wished to implement Access Control Lists (ACL’s) with different roles in order to have privilege structure, which in my opinion should be a must in every kind of application. 

•  How can you know that you security is good enough? (traceability)

Hard to define something as secured enough, cyber security is a never-ending field of new technologies being developed and different ways of people thinking and coming up with new attacks and hopefully ways to get around those attacks. I would define it in terms of minimizing the attack window, covering at least the most common vulnerabilities to prevent SQLI, XSS, etc. Furthermore, since any application can have security breaches, traceability is a vital feature. Traceability is very key, because if a security breach were to happen, we could trace back the source of the attack and hopefully unravel whatever damage the attacker did. Prevention is still the best way to go about cyber security instead of a reactionary kind of approach.

