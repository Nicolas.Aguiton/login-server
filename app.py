from http import HTTPStatus
from flask import Flask, abort, request, send_from_directory, make_response, render_template, session, redirect, url_for#Added session & redirect.
import flask_login
from flask_login import logout_user, current_user, decode_cookie
from werkzeug.datastructures import WWWAuthenticate
import flask
from login_form import LoginForm
from password_handling import conn
from login_form import signupForm
from json import dumps, loads
from base64 import b64decode
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token;
from threading import local
from markupsafe import escape
from datetime import timedelta #Added this as well

import password_handling # DB for users(Added this.)

tls = local()
inject = "'; insert into messages (sender,message) values ('foo', 'bar');select '"
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = None

    
# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
app.secret_key = 'mY s3kritz'
app.permanent_session_lifetime = timedelta(days = 1)#Added this as well.

# Add a login manager to the app. 
import flask_login
from flask_login import login_required, login_user
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
#Check if user exists/Password is correct.
class User(flask_login.UserMixin):
    pass



# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    if len(password_handling.getUserInfo(user_id)) == 0:
       return  # user?
    #if conn.cursor().execute(F'SELECT COUNT(*) FROM users where username = ?', (user_id,)).fetchone()[0] == 0:
     #   return
    # For a real app, we would load the User from a database or something
    user = User()
    user.id = user_id
    return user


# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        print(f'Basic auth: {uid}:{passwd}')
       # u = users.get(uid)
        if password_handling.check_pw(uid, passwd): # and check_password(u.password, passwd):
            return user_loader(uid)
    elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        print(f'Bearer auth: {auth_params}')
        for user in password_handling.getUserInfo("*"):
            if user[4] == auth_params:
                return user_loader(uid)
    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))

def pygmentize(text):
    """
    It takes a string of SQL code, and returns a string of HTML code that highlights the SQL code
    
    :param text: The text to be highlighted
    :return: A string of HTML that contains the highlighted SQL code.
    """
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap = True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')


#@app.route("/")
@app.route("/", methods = ['GET', 'POST'])
def signup():
    form = signupForm()
    #error = ""
    if request.method == "POST":
        print("in if")
        username = request.form.get("uname")  #.lower()
        password = request.form.get("pname") 
        print(f"SIGNUUP{username}{password}")
        addingNewUser = password_handling.addingNewUser(username, password)
        return redirect(flask.url_for('login'))
    return render_template('signup.html', form=form) #error = error

@app.route('/index')
@login_required
def index_html():
    """
    It renders the index.html file in the templates folder.
    :return: The index.html file is being returned.
    """
    return render_template("index.html")


@app.route('/login', methods=['GET', 'POST'])
def login():
    """
    If the form is valid, check if the password is correct, if it is, log the user in and redirect them
    to the index page
    :return: The login page is being returned.
    """
    form = LoginForm()
    if form.validate_on_submit():
        username = request.form.get("uname")
        password = request.form.get("pname")
        print(username)
        if password_handling.check_pw(username, password): 
            user = user_loader(username)
            #automatically sets logged in session cooki
            login_user(user)
            print(user)
            flask.flash('Logged in successfully.')
            next = flask.request.args.get('next')
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)
    
            return flask.redirect(next or flask.url_for('index_html')) 
    return render_template('./login.html', form=form)


@app.route("/logout")
def logout():
    """
    It logs out the user.
    :return: a redirect to the login page.
    """   
    logout_user()
    return redirect(flask.url_for("login"))


@app.get('/search')
def search():
    """
    It takes a query string parameter called q, and uses it to search the messages table for rows that
    match the query
    :return: The result of the query.
    """
    query = request.args.get('q') or request.form.get('q') or '*'
    stmt = f"SELECT * FROM messages WHERE message GLOB '{query}'"
    result = f"Query: {pygmentize(stmt)}\n"
    try:
        c = conn.execute(stmt)
        rows = c.fetchall()
        result = result + 'Result:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@app.route('/send', methods=['POST','GET']) 
@login_required
def send():
    """
    It takes the sender, receiver, timestamp, and message from the user and inserts it into the
    database.
    :return: The return value is a JSON object.
    """
    try:
        sender = current_user.id
        receiver = request.args.get('message')
        timestamp = f"SELECT timestamp FROM messages" # USE datetime.now().strftime(timestamp_format)
        message = request.args.get('message')
        if not message:
            return '{"error: "Missing message."}'

        if not receiver:
            receiver = 'everyone'
        else: 
            receiver_exists = conn.cursor().execute('SELECT 1 FROM users WHERE username=?', (receiver,)).fetchone()
            if not receiver_exists:
                return '{"Error: "Could not find who you are looking for.!"}'
            else:
                receiver = receiver.lower() # remove this maybe
        if receiver == sender:
            return '{"error: Why are you sending messages to yourself buddy?, get outta here!"}'

        stmt = F"INSERT INTO messages (sender, receiver, message, timestamp) VALUES (?,?,?,?)'" #REMOVE TIMESTAMP IF FAIL.
        conn.execute(stmt, (sender, receiver, message, timestamp))
        return "{}"
    except Error as e:
        return '{"error":' + f'"{e}"'+'}'

"""
unfinished
@app.get('/messages/ID')
@login_required
def fetch():
    try:
        msg_id = request.args.get('id')
        if not msg_id:
            max_msgId = conn.cursor().execute("SELECT MAX(id) FROM messages").fetchone()[0]
            return '{"msg_ID": "' + str(max_msgId) + '"}'

        msg_id, sender, receiver, message, timestamp = conn.cursor().execute('SELECT * FROM messages WHERE id = ? AND (receiver IN ("everyone", ?) O sender =?)', (msg_id, current_user.id, current_user.id)).fetchone()
        return '{"msg_ID: "' + str(msg_id) +'", "sender: "' + sender + '", "receiver": "'+receiver+'", "message": "'+message + '", "timestamp": "'+timestamp+ '"}'
    except Error as e:
        return f"ERROR: {e}"

"""

# Find out what the fuck this does? 
@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}

if __name__ == '__main__':
    app.run(debug=True)
